#!/bin/sh

##  This Source Code Form is subject to the terms of the Mozilla Public
##  License, v. 2.0. If a copy of the MPL was not distributed with this
##  file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
trap 'exit 128' INT
export PATH

script_path="${0}"
script_root="$(dirname "${script_path}")"

echo "[INFO] Project root: ${script_root}"
cd "${script_root}"

build_root="$(pwd)"/build

[ -e "${build_root}" ] && rm -f -r "${build_root}"

mkdir -p "${build_root}"
cp -r ../offensive_classifier_service "${build_root}"

docker build                                        \
       --file=Containerfile "$(pwd)"                \
       --tag offensive-classifier-service:latest    \
       "${@}"
