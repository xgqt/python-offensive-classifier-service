#!/usr/bin/env python3


"""
File checker business logic module.
""" """
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


import os
import uuid

from datetime import datetime

import opennsfw2


def check_received_file(file_bytes, file_name=""):
    """File checker"""

    checker_temp_path = "/tmp/checker_temp"

    if not os.path.exists(checker_temp_path):
        os.makedirs(checker_temp_path)

    file_uuid = str(uuid.uuid4())
    file_path = os.path.join(checker_temp_path, f"{file_uuid}_{file_name}")

    with open(file_path, "wb") as opened_file:
        opened_file.write(file_bytes)

    try:
        image_prediction = opennsfw2.predict_image(file_path)
        image_percent = int(round(image_prediction, 2) * 100)
    except Exception:
        print("check_received_file: could not identify, assigning max score")
        image_percent = 100

    try:
        os.remove(file_path)
    except Exception:
        pass

    return {"score": image_percent, "created_at": datetime.now()}
