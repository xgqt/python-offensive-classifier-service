#!/usr/bin/env python3


"""
""" """
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


from fastapi import FastAPI

from .routes import check_route


app = FastAPI()

app.include_router(check_route.router)


@app.get("/")
async def root_get():
    return {"message": "Root."}


@app.get("/api")
async def api_get():
    return {"message": "API."}
