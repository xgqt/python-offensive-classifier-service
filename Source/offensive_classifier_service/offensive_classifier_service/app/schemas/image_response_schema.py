"""
Pydantic model schema for image response schema.
"""


from datetime import datetime as DateTime
from pydantic import BaseModel


class ImageResponseSchema(BaseModel):
    score: int

    created_at: DateTime

    class Config:
        orm_mode = True
