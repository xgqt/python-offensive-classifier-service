#!/usr/bin/env python3


"""
Pydantic model schema for image request schema.
""" """
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


from datetime import datetime as DateTime
from pydantic import BaseModel


class ImageRequestSchema(BaseModel):
    score: int

    created_at: DateTime

    class Config:
        orm_mode = True
