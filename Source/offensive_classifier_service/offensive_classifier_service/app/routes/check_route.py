#!/usr/bin/env python3


"""
Routes for checking images.
""" """
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


from fastapi import APIRouter, UploadFile

from ..business_logic.checker import check_received_file
from ..schemas.image_response_schema import ImageResponseSchema


router = APIRouter(prefix="/api/check", tags=["check"])


@router.post("/", response_model=ImageResponseSchema)
async def check_route_post(post_file: UploadFile):
    """This is the API endpoint that consumes a multipart/form-data image file
    and ranks the image based on the unsafety score.

    It responds with a JSON object which contains two slots:
    1. "score" - describing how "unsafe" the consumed image is to the detection
    algorithm,
    2. "created_at" - timestamp then the response was produced.
    """

    file_bytes = await post_file.read()

    return check_received_file(file_bytes)
