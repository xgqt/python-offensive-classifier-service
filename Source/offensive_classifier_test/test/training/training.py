#!/usr/bin/env python3


"""
""" """
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


import os

import opennsfw2 as n2


def main():
    script_path = os.path.dirname(__file__)
    images_path = os.path.join(script_path, "images")

    images = os.listdir(images_path)

    for image in images:
        image_path = os.path.join(images_path, image)
        image_prediction = n2.predict_image(image_path)
        image_percent = int(round(image_prediction, 2) * 100)

        print(f'Image "{image}": {image_percent}%')


if __name__ == "__main__":
    main()
