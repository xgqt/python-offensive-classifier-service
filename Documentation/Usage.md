# Usage

## Run with Docker

Build script (`build.sh`) in `Source/offensive-classifier-service-container`
builds the image `offensive-classifier-service` (tagged as `latest`) which can
later be run with the `run.sh` script form the same directory.

```shell
cd Source/offensive-classifier-service-container
./build.sh
./run.sh
```

## REST API

Routes:

* POST: /api/check/ --- post a image to check the NSFW score

  Schema:

  ```json
  {
      "score": 0,
      "created_at": "2023-05-23T09:01:54.168Z"
  }
  ```

### Curl

Assuming `file.jpg` is in the current directory (and is readable).

```shell
curl -X POST                                    \
    'http://127.0.0.1:8000/api/check/'          \
    -H 'Content-Type: multipart/form-data'      \
    -F 'post_file=@file.jpg;type=image/jpeg'
```
