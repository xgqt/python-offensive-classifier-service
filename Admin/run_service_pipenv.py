#!/usr/bin/env python3


"""

""" """

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


from os import chdir
from os import path
from subprocess import run
from sys import argv


def main():
    """!
    Main.
    """

    script_path = path.realpath(__file__)
    script_root = path.dirname(script_path)

    leftover_args = argv[1::]

    offensive_classifier_service_path = path.realpath(
        path.join(script_root, "../Source/offensive_classifier_service")
    )
    pipenv_run_comamnd_arguments = [
        "pipenv",
        "run",
        f"{offensive_classifier_service_path}/run.sh",
        "--reload",
    ] + leftover_args

    print(f" * Entering directory: {offensive_classifier_service_path}")
    chdir(offensive_classifier_service_path)

    run(["pipenv", "install"], check=True)

    try:
        run(pipenv_run_comamnd_arguments, check=True)
    except KeyboardInterrupt:
        print(" * Received shutdown signal.")


if __name__ == "__main__":
    main()
