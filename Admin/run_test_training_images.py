#!/usr/bin/env python3


"""

""" """

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


from os import chdir
from os import path
from subprocess import run


def main():
    """!
    Main.
    """

    script_path = path.realpath(__file__)
    script_root = path.dirname(script_path)

    offensive_classifier_test_path = path.realpath(
        path.join(script_root, "../Source/offensive_classifier_test")
    )

    print(f" * Entering directory: {offensive_classifier_test_path}")
    chdir(offensive_classifier_test_path)

    run(["pipenv", "install"], check=True)
    run(["pipenv", "run", "./test/training/training.py"], check=True)


if __name__ == "__main__":
    main()
